package com.celtu.medual

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
class MedualApplication

fun main(args: Array<String>) {
	runApplication<MedualApplication>(*args)
}
