package com.celtu.medual.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory


/* Created by celtu on 07.05.2020 */
@Configuration
class HttpConfig {
    @Bean
    fun httpRequestFactory(): OkHttp3ClientHttpRequestFactory {
        return OkHttp3ClientHttpRequestFactory();
    }
}