package com.celtu.medual.domain

import javax.persistence.*

@Entity
@Table(name = "doctors")
class Doctor(
        var name: String,
        var surname: String,
        var specialization: String,
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null
)
