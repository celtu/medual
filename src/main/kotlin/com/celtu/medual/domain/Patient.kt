package com.celtu.medual.domain

import javax.persistence.*


/* Created by celtu on 06.05.2020 */
@Entity
@Table(name = "patients")
class Patient(
        var name: String,
        var surname: String,
        var address: String,
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null,
        @OneToMany(mappedBy = "patient")
        val visits: List<Visit>? = null
)