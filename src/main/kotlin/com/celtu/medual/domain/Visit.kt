package com.celtu.medual.domain

import java.time.ZonedDateTime
import javax.persistence.*


/* Created by celtu on 06.05.2020 */
@Entity
@Table(name = "visits")
class Visit(
        @ManyToOne
        @JoinColumn(name = "doctor_id", referencedColumnName = "id")
        var doctor: Doctor,
        @ManyToOne
        @JoinColumn(name = "patient_id", referencedColumnName = "id")
        var patient: Patient,
        @Column(name = "visit_date")
        var visitDate: ZonedDateTime?,
        var address: String,
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int? = null
)