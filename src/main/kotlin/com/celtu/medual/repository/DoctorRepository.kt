package com.celtu.medual.repository

import com.celtu.medual.domain.Doctor
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource


/* Created by celtu on 05.05.2020 */
@RepositoryRestResource(collectionResourceRel = "doctors", path = "doctors")
interface DoctorRepository : CrudRepository<Doctor, Int> {
}