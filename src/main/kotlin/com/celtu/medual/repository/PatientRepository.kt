package com.celtu.medual.repository

import com.celtu.medual.domain.Patient
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource


/* Created by celtu on 06.05.2020 */
@RepositoryRestResource(collectionResourceRel = "patients", path = "patients")
interface PatientRepository : CrudRepository<Patient, Int> {
}