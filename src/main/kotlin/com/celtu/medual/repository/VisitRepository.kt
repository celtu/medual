package com.celtu.medual.repository

import com.celtu.medual.domain.Visit
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource


/* Created by celtu on 06.05.2020 */
@RepositoryRestResource(collectionResourceRel = "visits", path = "visits")
interface VisitRepository : CrudRepository<Visit, Int> {
}