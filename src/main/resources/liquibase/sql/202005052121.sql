
create or replace table medual.doctors
(
    id int auto_increment
        primary key,
    name varchar(255) null,
    surname varchar(255) null,
    specialization varchar(10000) null
);

create or replace table medual.patients
(
    id int auto_increment
        primary key,
    name varchar(255) not null,
    surname varchar(255) not null,
    address varchar(10000) null
);

create or replace table medual.visits
(
    id int auto_increment
        primary key,
    doctor_id int not null,
    patient_id int not null,
    visit_date timestamp default current_timestamp() not null on update current_timestamp(),
    address varchar(10000) null,
    constraint visit_doctor_id_fk
        foreign key (doctor_id) references doctors (id),
    constraint visit_patient_id_fk
        foreign key (patient_id) references patients (id)
);

