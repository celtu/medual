alter table doctors modify name varchar(255) not null;

alter table doctors modify surname varchar(255) not null;

alter table doctors modify specialization varchar(10000) not null;