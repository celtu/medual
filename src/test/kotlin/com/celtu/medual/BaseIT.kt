package com.celtu.medual

import com.celtu.medual.repository.DoctorRepository
import com.celtu.medual.repository.PatientRepository
import com.celtu.medual.repository.VisitRepository
import com.celtu.medual.util.TestHelper
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate


/* Created by celtu on 07.05.2020 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class BaseIT() {

    val container = TestContainer.instance
    val helper = TestHelper()
//
    @Autowired
    lateinit var restTemplate: TestRestTemplate

    @Autowired
    lateinit var om: ObjectMapper;

    @Autowired
    lateinit var doctorRepository: DoctorRepository

    @Autowired
    lateinit var patientRepository: PatientRepository

    @Autowired
    lateinit var visitRepository: VisitRepository

    @BeforeEach
    fun baseInit() {
        cleanupTables()
    }

    private fun cleanupTables() {
        visitRepository.deleteAll()
        doctorRepository.deleteAll()
        patientRepository.deleteAll()
    }
}