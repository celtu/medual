package com.celtu.medual

import com.celtu.medual.domain.Doctor
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus


/* Created by celtu on 07.05.2020 */
class DoctorIT : BaseIT() {

    private lateinit var doctor1: Doctor

    @BeforeEach
    fun init() {
        doctor1 = doctorRepository.save(helper.doctor(1))
    }

    @Test
    fun `should retrieve doctor`() {
        val response = restTemplate.getForEntity("/doctors/${doctor1.id}", Doctor::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
        Assertions.assertThat(response.body)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(doctor1)
    }

    @Test
    fun `should store and return new doctor`() {
        val payload = helper.doctor(2)
        val response = restTemplate.postForEntity("/doctors/", payload, Doctor::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.CREATED)
        helper.assertReturnedAndStoredEntity(response, payload, doctorRepository, "id")
    }

    @Test
    fun `should update doctor data`() {
        val payload = helper.doctor(11)
        val response = restTemplate.exchange("/doctors/${doctor1.id}", HttpMethod.PUT, HttpEntity(payload), payload.javaClass);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
        helper.assertReturnedAndStoredEntity(response, payload, doctorRepository, "id")
    }

    @Test
    fun `should delete doctor`() {
        val response = restTemplate.exchange("/doctors/${doctor1.id}", HttpMethod.DELETE, HttpEntity.EMPTY, Doctor::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.NO_CONTENT)
        Assertions.assertThat(doctorRepository.findById(doctor1.id!!)).isNotPresent
    }

    @Test
    fun `should not allow create doctor without name surname specialization`() {
        val response = restTemplate.postForEntity("/doctors/", om.createObjectNode(), String::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.BAD_REQUEST)
    }
}