package com.celtu.medual

import com.celtu.medual.domain.Patient
import com.fasterxml.jackson.databind.node.ObjectNode
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus


/* Created by celtu on 07.05.2020 */
class PatientIT : BaseIT() {

    private lateinit var patient1: Patient
    private lateinit var patient2: Patient

    @BeforeEach
    fun init() {
        patient1 = patientRepository.save(helper.patient(1))
        patient2 = patientRepository.save(helper.patient(2))
    }

    @Test
    fun `should retrieve patient`() {
        val response = restTemplate.getForEntity("/patients/${patient1.id}", Patient::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
        Assertions.assertThat(response.body)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(patient1)
    }

    @Test
    fun `should store and return new patient`() {
        val payload = helper.patient(2)
        val response = restTemplate.postForEntity("/patients/", payload, Patient::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.CREATED)
        helper.assertReturnedAndStoredEntity(response, payload, patientRepository, "id", "visits")
    }

    @Test
    fun `should update patient data`() {
        val payload = Patient("name1", "surname1", "address1")
        val response = restTemplate.exchange("/patients/${patient1.id}", HttpMethod.PUT, HttpEntity(payload), payload.javaClass)
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
        helper.assertReturnedAndStoredEntity(response, payload, patientRepository, "id", "visits")
    }

    @Test
    fun `should delete patient`() {
        val response = restTemplate.exchange("/patients/${patient1.id}", HttpMethod.DELETE, HttpEntity.EMPTY, Patient::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.NO_CONTENT)

        Assertions.assertThat(patientRepository.findById(patient1.id!!)).isNotPresent
    }

    @Test
    fun `should retrieve all patients`() {
        val response = restTemplate.getForEntity("/patients/", ObjectNode::class.java)
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
        Assertions.assertThat(response.body?.get("_embedded")?.get("patients")).hasSize(2)
    }
}