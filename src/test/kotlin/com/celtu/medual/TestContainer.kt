package com.celtu.medual

import org.slf4j.LoggerFactory
import org.testcontainers.containers.DockerComposeContainer
import org.testcontainers.containers.wait.strategy.Wait
import java.io.File
import java.io.IOException
import java.net.Socket
import java.time.Duration

object TestContainer {
    val log = LoggerFactory.getLogger(TestContainer::class.java)

    val instance by lazy {
        initContainer()
    }

    class KDockerComposeContainer(file: File) : DockerComposeContainer<KDockerComposeContainer>(file)

    private fun initContainer(): KDockerComposeContainer? {
        if (!isListening()) {
            log.info("Initializing TestContainer...")
            var instance = KDockerComposeContainer(File(this::class.java.getResource("/docker/medual-dev/docker-compose.yml").file)).apply {
                withExposedService("mariadb", 3306, Wait.forListeningPort().withStartupTimeout(Duration.ofSeconds(30)))
                start()
            }
            log.info("TestContainer initialized")
            return instance;
        } else {
            log.info("Skip TestContainer initialization as it is already running")
            return null;
        }
    }

    private fun isListening(): Boolean {
        try {
            val socket = Socket("localhost", 32001)
            socket.use {
                return true;
            }
        } catch (ioe: IOException) {
            return false;
        }
    }
}