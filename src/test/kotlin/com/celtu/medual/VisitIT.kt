package com.celtu.medual

import com.celtu.medual.domain.Doctor
import com.celtu.medual.domain.Patient
import com.celtu.medual.domain.Visit
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.time.Month
import java.time.ZoneId
import java.time.ZonedDateTime


class VisitIT : BaseIT() {

    private lateinit var doctor1: Doctor
	private lateinit var patient1: Patient
	private lateinit var patient2: Patient
	private lateinit var visit1: Visit
	private lateinit var visit2: Visit
	private lateinit var visit3: Visit

	@BeforeEach
	fun init() {
		doctor1 = doctorRepository.save(helper.doctor(1))
		patient1 = patientRepository.save(helper.patient(1))
		patient2 = patientRepository.save(helper.patient(2))
		visit1 = visitRepository.save(Visit(doctor1, patient1, ZonedDateTime.of(LocalDateTime.of(2020, Month.MAY, 20, 18, 30), ZoneId.systemDefault()), "addres1"))
		visit2 = visitRepository.save(Visit(doctor1, patient2, ZonedDateTime.of(LocalDateTime.of(2020, Month.MAY, 20, 19, 0), ZoneId.systemDefault()), "addres1"))
		visit3 = visitRepository.save(Visit(doctor1, patient1, ZonedDateTime.of(LocalDateTime.of(2020, Month.MAY, 27, 18, 30), ZoneId.systemDefault()), "addres1"))
	}

	@Test
	fun `should store new visit along with relationships`() {
		val visitDateString = "2020-05-20T18:30+02:00"
		val json: JsonNode = om.createObjectNode()
				.put("address", "address")
				.put("visitDate", visitDateString) // 2020-05-20T18:30+02:00[Europe/Warsaw]
				.put("doctor", "/doctors/${doctor1.id}")
				.put("patient", "/patients/${patient1.id}")
		val response = restTemplate.postForEntity("/visits/", json, JsonNode::class.java);
		Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.CREATED)
		// body
		val visitDate = ZonedDateTime.parse(response.body?.get("visitDate")?.textValue())
		Assertions.assertThat(visitDate).isEqualTo(visitDateString)
		Assertions.assertThat(response.body?.get("address")?.textValue()).isEqualTo("address")
		// doctor body link
		val doctorLink = response.body?.get("_links")?.get("doctor")?.get("href")?.textValue()
		Assertions.assertThat(doctorLink).isNotBlank()
		val doctorResponse = restTemplate.getForEntity(doctorLink, Doctor::class.java)
		Assertions.assertThat(doctorResponse.body?.name).isEqualTo(doctor1.name)
		// patient body link
		val patientLink = response.body?.get("_links")?.get("patient")?.get("href")?.textValue()
		Assertions.assertThat(patientLink).isNotBlank()
		val patientResponse = restTemplate.getForEntity(patientLink, Patient::class.java)
		Assertions.assertThat(patientResponse.body?.name).isEqualTo(patient1.name)
		// db
		val visitId = helper.extractResourceId(response);
		val stored: Visit = visitRepository.findById(visitId!!).get()
		Assertions.assertThat(stored).isNotNull
		Assertions.assertThat(stored.address).isEqualTo("address")
		Assertions.assertThat(stored.visitDate).isEqualTo(visitDateString)
		Assertions.assertThat(stored.doctor.id).isEqualTo(doctor1.id)
		Assertions.assertThat(stored.patient.id).isEqualTo(patient1.id)
	}

    @Test
    fun `should delete visit`() {
        val response = restTemplate.exchange("/visits/${visit1.id}", HttpMethod.DELETE, HttpEntity.EMPTY, Doctor::class.java);
        Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.NO_CONTENT)
        Assertions.assertThat(visitRepository.findById(visit1.id!!)).isNotPresent
    }

	@Test
	fun `should update visit date`() {
		val nextAvailableVisitDate = "2023-03-23T12:30+02:00"
		val payload = om.createObjectNode().put("visitDate", nextAvailableVisitDate)
		val response = restTemplate.exchange("/visits/${visit1.id}", HttpMethod.PATCH, HttpEntity(payload), payload.javaClass);
		Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
		val visitDate = ZonedDateTime.parse(response.body?.get("visitDate")?.textValue())
		Assertions.assertThat(visitDate).isEqualTo(nextAvailableVisitDate)
		//db
		val stored = visitRepository.findById(visit1.id!!).get()
		Assertions.assertThat(stored.visitDate).isEqualTo(nextAvailableVisitDate)
	}

	@Test
	fun `should retrieve all visits`() {
		val response = restTemplate.getForEntity("/visits/", ObjectNode::class.java)
		Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
		Assertions.assertThat(response.body?.get("_embedded")?.get("visits")).hasSize(3)
	}

	@Test
	fun `should return just visits of patient1`() {
		val response = restTemplate.getForEntity("/patients/${patient1.id}/visits/", ObjectNode::class.java)
		Assertions.assertThat(response.statusCode).isEqualByComparingTo(HttpStatus.OK)
		Assertions.assertThat(response.body?.get("_embedded")?.get("visits")).hasSize(2)
	}

	@Test
	fun `should display current zone when visit requested from different one`() {
		val visitDateString = "2020-05-20T23:30+07:00" // 2020-05-20T18:30+02:00[Europe/Ostrów], +07:00 - e.g. Vietnam
		val json: JsonNode = om.createObjectNode()
				.put("address", "address")
				.put("visitDate", visitDateString)
				.put("doctor", "/doctors/${doctor1.id}")
				.put("patient", "/patients/${patient1.id}")
		val response = restTemplate.postForEntity("/visits/", json, JsonNode::class.java);
		val visitDate = ZonedDateTime.parse(response.body?.get("visitDate")?.textValue())
		Assertions.assertThat(visitDate).isEqualTo("2020-05-20T23:30+07:00")
	}

}
