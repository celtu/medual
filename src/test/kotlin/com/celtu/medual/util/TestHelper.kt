package com.celtu.medual.util

import com.celtu.medual.domain.Doctor
import com.celtu.medual.domain.Patient
import org.assertj.core.api.Assertions
import org.springframework.data.repository.CrudRepository
import org.springframework.http.ResponseEntity


/* Created by celtu on 07.05.2020 */
class TestHelper {

    /**
     * Verify whether request payload fields have been applied to http response body & database.
     * Likely reusable when request payload representation reflects response/database entity 1to1.
     */
    fun <T> assertReturnedAndStoredEntity(response: ResponseEntity<T>, payload: T, repository: CrudRepository<T, Int>, vararg fieldsToIgnore: String) {
        // body
        val body = response.body
        Assertions.assertThat(body)
                .isNotNull
                .usingRecursiveComparison()
                .isEqualTo(payload)
        val resourceId = extractResourceId(response)
        Assertions.assertThat(resourceId).isNotNull()
        // db
        val stored = repository.findById(resourceId!!).get()
        Assertions.assertThat(stored)
                .isNotNull
                .usingRecursiveComparison()
                .ignoringFields(*fieldsToIgnore)
                .isEqualTo(payload)
    }

    fun <T> extractResourceId(response: ResponseEntity<T>) =
            response.headers.get("Location")?.get(0)?.substringAfterLast("/")?.toInt()

    fun doctor(no: Int) = Doctor("name$no", "śurname$no", "śpęc$no")

    fun patient(no: Int) = Patient("name$no", "surname$no", "äddręś$no")

}